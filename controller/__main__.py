import paho.mqtt.client as mqtt
import serial
import json
import glob
import os

mqtt_address = os.environ.get("MQTT_ADDRESS", "localhost")

ser_file = glob.glob('/dev/ttyUSB*')
print("Found", ser_file)

ser = serial.Serial(ser_file[0], baudrate=57600)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("room/light/#")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    payload = {}
    msg.payload = msg.payload.decode("utf-8")
    print(msg.topic, ":", msg.payload)

    if msg.topic.endswith("brightness"):
        payload["brightness"] = int(msg.payload)
    elif msg.topic.endswith("pattern"):
        payload["pattern"] = msg.payload
    elif msg.topic.endswith("colour"):
        colour = msg.payload.replace("rgb(", "").replace(")", "").split(",")
        payload["colour"] = colour

    print(payload)
    ser.write(str.encode(json.dumps(payload)))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

print("Connecting to", mqtt_address)
client.connect(mqtt_address, 1883, 60)

client.loop_forever()
