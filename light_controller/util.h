float interpolate(float target, float current, float steps = 25) {
  return (target - current) / steps;
}
