#pragma once
#include "util.h"

const int ANIMATION_SPEED = 5;

class Light {
    float brightness = 0;
    float target_brightness = 255;
    int pin;

  public:
    Light() {}
    Light(int pin, int brightness = 0) {
      this->pin = pin;
      this->brightness = brightness;
    }

    void update() {
      if (abs(brightness - target_brightness) > 1) {
        brightness += interpolate(target_brightness, brightness);
      }

      analogWrite(pin, brightness);
    }

    void set_target(float value) {
      target_brightness = value;
    }
};

Light light;