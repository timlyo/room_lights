#include "pattern.h"
#include "colour.h"

#define LED_COUNT 20
#define PIN 6

const float COEFFICIENT = 255 / LED_COUNT;

CRGB leds[LED_COUNT];
int target_brightness = 255;

void rainbow();

class IndvLedStrip {
  public:
    static void init() {
      Serial.println("Creating strip");
      FastLED.addLeds<WS2811, PIN, BRG>(leds, LED_COUNT).setCorrection(TypicalLEDStrip);

      for (int i = 0; i < LED_COUNT; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
    }

    static void update() {
      //Pattern
      switch (pattern) {
        case Pattern::Solid:
          solid();
          break;
        case Pattern::Rainbow:
          rainbow();
          break;
      }

      FastLED.show();

      //brightness
      float current_brightness = FastLED.getBrightness();
      if (abs(current_brightness - target_brightness) > 1) {
        float increment = interpolate(target_brightness, current_brightness);
        FastLED.setBrightness(current_brightness + increment);
      }
    }

    static void brightness(int brightness) {
      target_brightness = brightness;
    }

  private:
    static void rainbow() {
      static float offset = 0;
      int spectrum_fraction = 2;
      offset = (offset + 0.2);
      if (offset > 255) {
        offset = 0;
      }

      for (int i = 0; i < LED_COUNT; i++) {
        uint8_t hue = ((i + offset) * COEFFICIENT) / spectrum_fraction;
        leds[i] = CHSV(hue, 255, 255);
      }
    }

    static void solid() {
      for (int i = 0; i < LED_COUNT; i++) {
        leds[i] = current_colour;
      }
    }
};
