#pragma once

enum class Pattern {
  Solid,
  Rainbow,
  Noisy,
  Breathe,
  Strobe,
  Candle
};

Pattern pattern_from_string(String new_pattern){
    if(new_pattern == "Rainbow")
        return Pattern::Rainbow;
    else if(new_pattern =="Noisy")
        return Pattern::Noisy;
    else if(new_pattern == "Breathe")
        return Pattern::Breathe;
    else if (new_pattern == "Strobe")
        return Pattern::Strobe;
    else if (new_pattern == "Candle")
        return Pattern::Candle;
    else if(new_pattern == "Solid")
        return Pattern::Solid;

    Serial.print("Pattern not recognised");
    Serial.println(new_pattern);

    return Pattern::Solid;
}

Pattern pattern = Pattern::Solid;
