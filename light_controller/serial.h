#pragma once

#include <ArduinoJson.h>
#include "pattern.h"
#include "light.h"

String read_serial() {
  if (Serial.available()) {
    String input = Serial.readStringUntil('\n');
    return input;
  }

  return "";
}

void handle_input(String input) {
  StaticJsonBuffer<200> jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(input);

  if (root.containsKey("brightness")) {
     Serial.print("Setting new brightness: ");
     int brightness = root["brightness"];
     Serial.println(brightness);
     light.set_target(brightness);
     IndvLedStrip::brightness(brightness);

  }else if(root.containsKey("colour")){
    JsonArray& colour = root["colour"].asArray();
    current_colour = CRGB(colour[0], colour[1], colour[2]);

  }else if(root.containsKey("pattern")){
    Serial.print("Setting new pattern: ");

    char* new_pattern = root["pattern"];
    Serial.println(new_pattern);
    pattern = pattern_from_string(new_pattern);
  }
}

