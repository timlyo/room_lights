#include <FastLED.h>
#include "light.h"
#include "indv_led_strip.h"
#include "serial.h"

void setup() {
  delay(1000);
  pinMode(11, OUTPUT);
  Serial.begin(57600);
  light = Light(11);
  IndvLedStrip::init();

  light.set_target(200);

  Serial.println("Started");
}

unsigned long previous_time = 0;
const unsigned long interval = 16;

void loop() {
  unsigned long current_time = millis();

  if (current_time - previous_time > interval) {
    light.update();
    IndvLedStrip::update();

    previous_time = current_time;
  }

  String input = read_serial();
  if (input != "") {
    handle_input(input);
  }
}
